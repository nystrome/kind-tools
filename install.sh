#!/bin/bash

bash <(curl -s https://gitlab.com/nystrome/kind-tools/-/raw/main/install-tools.sh)
bash <(curl -s https://gitlab.com/nystrome/kind-tools/-/raw/main/create-cluster.sh)