#!/bin/bash

echo 'creating kind cluster'
curl -Lo /tmp/kind-config.yaml https://gitlab.com/nystrome/kind-tools/-/raw/main/kind-config.yaml
kind create cluster --config=/tmp/kind-config.yaml
rm /tmp/kind-config.yaml

sleep 2
echo
echo 'installing nginx ingress controller'
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml

echo
echo 'waiting for install completion...'
sleep 15
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s

echo
echo 'cleaning up install jobs'
kubectl delete jobs.batch --namespace ingress-nginx --all


echo 'removing ingress hook administration'
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission

sleep 2
echo 'cluster available'
